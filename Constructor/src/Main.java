
public class Main {

	public static void main(String[] args) {
	
		
		BankAccount maciejAccount = new BankAccount(12345, 1000.00, "Maciej", "mac@mac", 999);
		
		System.out.println("acc number: " + maciejAccount.getAccountNumber() 
							+"\nmoney: " + maciejAccount.getBalance()
							+"\ncustomer: " + maciejAccount.getCustomerName()
							+"\nemail: " + maciejAccount.getEmail()
							+"\nphone: " +maciejAccount.getPhoneNumber());
		
		/*maciejAccount.setAccountNumber(1234567890);
		maciejAccount.setCustomerName("Maciej Slawinski");
		maciejAccount.setEmail("mac.sawinski@");
		maciejAccount.setPhoneNumber(7981232);*/
		
		maciejAccount.deposit(150.00);
		maciejAccount.withdrawal(100.00);
		
		VipCustomer michalAccount = new VipCustomer();
		System.out.println("VIP name: " + michalAccount.getName() + 
				" creditlimit: " + michalAccount.getCreditLimit() +
				" email: " + michalAccount.getEmail());
		
		

	}

}
