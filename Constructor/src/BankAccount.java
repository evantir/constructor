
public class BankAccount {

	private int accountNumber;
	private double balance;
	private String customerName;
	private String email;
	private int phoneNumber;
	
	public BankAccount(){
		System.out.println("empty");
	}
	
	public BankAccount(int accountNumber, double balance, String customerName, String email, int phoneNumber){
		this.accountNumber = accountNumber;
		this.balance = balance;
		this.customerName = customerName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		//save values
	}
	
	
	
	public BankAccount(String customerName, String email, int phoneNumber) {
		this(999, 10.10, customerName, email, phoneNumber);
	
	}

	public void deposit(double depositAmount){
		this.balance += depositAmount;
		System.out.println("new balance is " + this.balance);
		
	}
	
	public void withdrawal(double withdrawalAmount){
		if(this.balance - withdrawalAmount < 0){
			System.out.println("wrong! only "+ this.balance + " available");
		}
		else
			this.balance -= withdrawalAmount;
		System.out.println("remaning balance = " + this.balance);
	}
	
	
	
	public int getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(int phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	
}
